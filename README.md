These are, basically, my lecture notes on the subject of basic algorithms used
in the programming competitions in Serbia (and IOI).

It's written in Serbian with algorithms in pseudocode, C++ (with light C++11
useage, since the competitions in programming guarantee gcc 4.8.1 which has full
C++11 implementation) and a bit of Haskell for good measure (code is, naturally,
written in English).
